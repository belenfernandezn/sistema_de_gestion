#ifndef TURNOS_H_INCLUDED
#define TURNOS_H_INCLUDED

#include "Fecha.h"

class Turno {
private:
    Fecha fecha;
    int horario;
    int Cod_Profesor;
    int Cod_Alumno;
public:
    Turno();

    void setFecha(Fecha);

    void setHorario(int horario);

    void setCod_Profesor(int CodProfesor);

    void setCod_Alumno(int CodAlumno);

    Fecha getFecha();

    int getHorario();

    int getCodigoProfesor();

    int getCodigoAlumno();

    //Files
    bool leerDeDisco(int pos);

    bool grabarEnDisco(Turno turno);

    void mostrarTurnos();
};

#endif // TURNOS_H_INCLUDED
