#ifndef FUNCIONES_H_INCLUDED
#define FUNCIONES_H_INCLUDED

#include "Alumno.h"

void menuProfesor();

void menuAlumno();

void menuDniExistente();

void menuModificarAlumno();

bool altaAlumno();

bool verificarDni(Alumno alumno, Alumno obj);

void verificarPersistencia(bool guardado);

void listarAlumnos();

void listarAlumnosActivos(Alumno alumnos);

void mostrarAlumno();

void consultarEstadoPago();

void listarAlumnosMorosos();

#endif // FUNCIONES_H_INCLUDED
