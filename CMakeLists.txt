cmake_minimum_required(VERSION 3.20)
project(DEMO_V2_0)

set(CMAKE_CXX_STANDARD 14)

include_directories(.)
include_directories(Nomina)

add_executable(DEMO_V2_0
        Nomina.cpp
        Nomina.h
        Alumno.cpp
        Alumno.h
        Fecha.cpp
        Fecha.h
        Funciones.cpp
        Funciones.h
        main.cpp
        Moroso.cpp
        Moroso.h
        Persona.cpp
        Persona.h
        Profesor.cpp
        Profesor.h
        Turno.cpp
        Turno.h)
