#include <iostream>
#include <cstdio>
#include "Alumno.h"

using namespace std;

Alumno::Alumno() { codAlumno = 0; };

void Alumno::setCodigo(int valor) { codAlumno = valor; };

void Alumno::setPeso(float valor) { peso = valor; };

void Alumno::setAltura(int valor) { altura = valor; };

void Alumno::setMesPago(bool valor) { mesPago = valor; };

int Alumno::getCodigo() { return codAlumno; };

float Alumno::getPeso() { return peso; };

int Alumno::getAltura() { return altura; };

bool Alumno::getMesPago() { return mesPago; };

int Alumno::getDeuda() { return deuda; };

void Alumno::mostrar() {
    Persona::mostrar();
    cout << "Codigo Alumno: " << codAlumno << endl;
    cout << "Peso: " << peso << endl;
    cout << "Altura: " << altura << endl;
    cout << "Mes pago: " << mesPago << endl;
    cout << endl;
}

void Alumno::cargar() {
    Persona::cargar();
    cout << "Codigo Alumno: ";
    cin >> codAlumno;
    cout << "Peso: ";
    cin >> peso;
    cout << "Altura: ";
    cin >> altura;
    mesPago = true;
}

bool Alumno::modificar(Alumno obj, int dni, int prop) {
    int pos = 0;
    bool escribio = false;

    FILE * pAlumnos;
    pAlumnos = fopen("Alumnos.dat", "rb+");
    if (pAlumnos == nullptr) {
        cout << "Error de archivo";
        return false;
    }

    while (obj.leerDeDisco(pos++)) {
        if (dni == obj.getDni()) {
            switch (prop) {
                case 1:
                    int codigo;
                    cout << "Nuevo codigo: ";
                    cin >> codigo;
                    obj.setCodigo(codigo);
                    break;
                case 2:
                    char nombre[30];
                    cout << "Nuevo nombre: ";
                    cin >> nombre;
                    obj.setNombre(nombre);
                    break;
                case 3:
                    char apellido[30];
                    cout << "Nuevo apellido: ";
                    cin >> apellido;
                    obj.setApellido(apellido);
                    break;
                case 4:
                    int dni;
                    cout << "Nuevo DNI: ";
                    cin >> dni;
                    obj.setDni(dni);
                    break;
                case 5:
                    int telefono;
                    cout << "Nuevo telefono: ";
                    cin >> telefono;
                    obj.setTelefono(telefono);
                    break;
                case 6:
                    int peso;
                    cout << "Nuevo Peso: ";
                    cin >> peso;
                    obj.setPeso(peso);
                    break;
                case 7:
                    int altura;
                    cout << "Nueva altura: ";
                    cin >> altura;
                    obj.setAltura(altura);
                    break;
                case 8:
                    char estado[9];
                    cout << "Estado (activo / inactivo): ";
                    cin >> estado;
                    if(estado == "activo") {
                        obj.setEstado(true);
                    } else {
                        obj.setEstado(false);
                    }
                    break;
                case 9:
                    char mesPago[2];
                    cout << "Mes pago (Si / No): ";
                    cin >> mesPago;
                    if(mesPago == "Si") {
                        obj.setMesPago(true);
                    } else {
                        obj.setMesPago(false);
                    }
                    break;
            }
            fseek(pAlumnos, ftell(pAlumnos) - sizeof obj, 0);
            escribio = fwrite(&obj, sizeof obj, 1, pAlumnos);
            fclose(pAlumnos);
            return escribio;
        }
    }
    fclose(pAlumnos);
    system("pause");
    return escribio;
}

bool Alumno::leerDeDisco(int pos) {
    bool ok;
    FILE *p;
    p = fopen("Alumnos.dat", "rb");
    if (p == nullptr) { return false; }
    fseek(p, sizeof(Alumno)*pos, 0);
    ok = fread(this, sizeof(Alumno), 1, p);
    fclose(p);
    return ok;
}

bool Alumno::grabarEnDisco(Alumno alumno) {
    FILE *p;
    p = fopen("Alumnos.dat", "ab");
    if (p == nullptr) { return false; }
    fwrite(&alumno, sizeof(Alumno), 1, p);
    fclose(p);
    return true;
}
