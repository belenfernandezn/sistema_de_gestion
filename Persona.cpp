#include <iostream>
#include <cstring>
#include "Persona.h"

using namespace std;

Persona::Persona() {}

void Persona::setNombre(char *nombre) { strcpy(nombre, nombre); }

void Persona::setApellido(char *apellido) { strcpy(apellido, apellido); }

void Persona::setDni(int value) { dni = value; };

void Persona::setTelefono(int value) { telefono = value; };

void Persona::setEstado(bool value) { estado = value; };

char *Persona::getNombre() { return nombre; };

char *Persona::getApellido() { return apellido; };

int Persona::getDni() { return dni; };

int Persona::getTelefono() { return telefono; };

bool Persona::getEstado() { return estado; };

void Persona::cargar() {
    cout << "Nombre: ";
    cin >> nombre;
    cout << "Apellido: ";
    cin >> apellido;
    cout << "DNI: ";
    cin >> dni;
    cout << "Telefono: ";
    cin >> telefono;
    estado = true;

}

void Persona::mostrar(){
    cout << "Nombre: " << nombre << endl;
    cout << "Apellido: " << apellido << endl;
    cout << "DNI: " << dni << endl;
    cout << "Telefono: " << telefono << endl;
    cout << "Estado: " << estado << endl;
}
