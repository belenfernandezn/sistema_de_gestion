#ifndef ALUMNO_H_INCLUDED
#define ALUMNO_H_INCLUDED
#include "Persona.h"

class Alumno : public Persona {
private:
    int codAlumno;
    float peso;
    int altura;
    bool mesPago;
    int deuda;
public:
    Alumno();

    void setCodigo(int codAlumno);

    void setPeso(float pesoAlumno);

    void setAltura(int AlturaAlumno);

    void setMesPago(bool MesPago);

    int getDeuda();

    int getCodigo();

    float getPeso();

    int getAltura();

    bool getMesPago();

    bool leerDeDisco(int pos);

    static bool grabarEnDisco(Alumno alumno);

    bool modificar(Alumno obj, int dni, int prop);

    void mostrar();

    void cargar();
};

#endif // ALUMNO_H_INCLUDED
