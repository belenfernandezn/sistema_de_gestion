#ifndef MOROSOS_H_INCLUDED
#define MOROSOS_H_INCLUDED

class Moroso{
private:
    int dni;
    int deuda;
    int cantMeses;
public:
    Moroso();

    void setDni(int codigo);

    void setDeuda(int cant);

    void setCantMeses(int cant);

    int getDni();

    int getDeuda();

    int getCantMeses();

    bool leerDeDisco(int pos);

    static bool grabarEnDisco(Moroso moroso);

    void mostrar(int morosoDni);
};

#endif // MOROSOS_H_INCLUDED
