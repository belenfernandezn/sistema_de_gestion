#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include "Alumno.h"
#include "Moroso.h"
#include "Funciones.h"

using namespace std;

void menuProfesor() {
    system("cls");
    int opcion;
    cout << "Gimnasio CHAU CHAU GRASITA" << endl;
    cout << "------------------------------" << endl << endl;
    cout << "PROFESORES" << endl;
    cout << "1- Alta" << endl;
    cout << "2- Modificacion" << endl;
    cout << "3- Baja temporal" << endl;
    cout << "4- Baja definitiva" << endl;
    cout << "5- Mostrar profesores" << endl;
    cout << "0- Volver al menu principal" << endl;
    cout << endl;
    cout << "Ingrese la opcion deseada: " << endl;
    cin >> opcion;

    switch (opcion) {
        case 0:
            break;
        default:
            cout << "La opcion ingresada no es valida" << endl;
            system("pause");
            break;
    }
}

void menuAlumno() {
    system("cls");
    int opcion;
    cout << "Gimnasio CHAU CHAU GRASITA" << endl;
    cout << "------------------------------" << endl << endl;
    cout << "ALUMNOS" << endl;
    cout << "1- Alta" << endl;
    cout << "2- Modificacion" << endl;
    cout << "3- Baja definitiva" << endl;
    cout << "4- Mostrar alumno" << endl;
    cout << "5- Consultar estado de pago" << endl;
    cout << "6- Mostrar listado de alumnos" << endl;
    cout << "7- Mostrar listado de alumnos morosos" << endl;
    cout << "0- Volver al menu principal" << endl;
    cout << endl;
    cout << "Ingrese la opcion deseada: ";
    cin >> opcion;

    switch (opcion) {
        case 1:
            altaAlumno();
            break;
        case 2:
            menuModificarAlumno();
            break;
        case 4:
            mostrarAlumno();
            break;
        case 5:
            consultarEstadoPago();
            break;
        case 6:
            listarAlumnos();
            break;
        case 7:
            listarAlumnosMorosos();
        case 0:
            break;
        default:
            cout << "La opcion ingresada no es valida" << endl;
            system("cls");
            break;
    }
}

bool altaAlumno() {
    bool dniExistente = false, guardado = false;
    Alumno alumno;
    Alumno obj;

    alumno.cargar();
    dniExistente = verificarDni(alumno, obj);

    if (dniExistente) {
        menuDniExistente();
        return false;
    } else {
        guardado = alumno.grabarEnDisco(alumno);
        verificarPersistencia(guardado);
        system("pause");
        return guardado;
    }
};

bool verificarDni(Alumno alumno, Alumno obj) {
    int pos = 0;
    while (obj.leerDeDisco(pos++)) {
        if (alumno.getDni() == obj.getDni()) {
            return true;
        }
    }
    return false;
}

void menuDniExistente() {
    int rta = -1;
    system("cls");
    cout << "El DNI del alumno que se desea dar de alta ya se encuentra en la base de datos" << endl;
    cout << endl;
    cout << "1- Volver a ingresar los datos";
    cout << "0- Volver al menu principal";
    cin >> rta;

    switch (rta) {
        case 1:
            altaAlumno();
            break;
        case 0:
            break;
        default:
            cout << "La opcion ingresada no es correcta" << endl;
            menuDniExistente();
            break;
    }
}

void verificarPersistencia(bool guardar) {
    if (guardar) {
        cout << "Alumno dado de alta exitosamente" << endl;
    } else {
        cout << "Error al guardar, intente nuevamente" << endl;
    }
}

void menuModificarAlumno() {
    int prop, dni;
    cout << "Ingrese la propiedad que desea modificar: " << endl;
    cout << "1- Codigo" << endl;
    cout << "2- Nombre" << endl;
    cout << "3- Apellido" << endl;
    cout << "4- DNI" << endl;
    cout << "5- Telefono" << endl;
    cout << "6- Peso" << endl;
    cout << "7- Altura" << endl;
    cout << "8- Baja temporal" << endl;
    cout << "9- Mes pago" << endl;
    cin >> prop;
    system("cls");

    Alumno obj;
    cout << "Ingrese el DNI del alumno: ";
    cin >> dni;

    obj.modificar(obj, dni, prop);
}

void consultarEstadoPago() {
    int dni, pos = 0;
    Alumno alumno;
    cout << "Ingrese el DNI del alumno: ";
    cin >> dni;

    while (alumno.leerDeDisco(pos++)) {
        if (alumno.getDni() == dni) {
            if (alumno.getMesPago()) {
                cout << "El alumno esta al dia con el pago" << endl;
            } else {
                cout << "El alumno no esta al dia con el pago" << endl;
            }
            system("pause");
            break;
        }
    }
}

void mostrarAlumno() {
    int dni, pos = 0;
    Alumno alumno;
    cout << "Ingrese el DNI del alumno: ";
    cin >> dni;

    while (alumno.leerDeDisco(pos++)) {
        if (alumno.getDni() == dni) {
            alumno.mostrar();
            system("pause");
            break;
        }
    }
}

void listarAlumnos() {
    Alumno alumno;
    int pos = 0, rta = 0;
    cout << "1- Ver solo los alumnos ACTIVOS" << endl;
    cout << "2- Ver todos los alumnos" << endl;
    cout << "Ingrese una opcion: ";
    cin >> rta;

    while (alumno.leerDeDisco(pos++)) {
        if (rta == 1) {
            listarAlumnosActivos(alumno);
        } else {
            alumno.mostrar();
        }
    }
    system("pause");
}

void listarAlumnosActivos(Alumno alumnos) {
    if (alumnos.getEstado()) {
        alumnos.mostrar();
    }
}

void listarAlumnosMorosos() {
    int pos = 0, i, totalMorosos = 0;
    std::vector<Moroso> morosos;
    Moroso moroso;
    FILE * pMorosos;
    pMorosos = fopen("Morosos.dat", "rb+");

    if (pMorosos == nullptr) {
        cout << "No hay morosos registrados";
        system("pause");
        return;
    }

    totalMorosos = ftell(pMorosos) / sizeof moroso;

    cout << "Total de morosos: " << totalMorosos << endl << endl;

    while (moroso.leerDeDisco(pos++)) {
        if (!moroso.getDeuda()) {
            morosos.push_back(moroso);
            break;
        }
    }

    std::vector<Moroso> morososOrdenados;
    int menor = morosos[0].getDeuda();
    for(i=1; i < totalMorosos; i++) {
        if (morosos[i].getDeuda() < menor) {
            menor = morosos[i].getDeuda();
            morososOrdenados[i-1] = morosos[i];
            morososOrdenados[i] = morosos[i-1];
        }
    }

    for (i=0; i < totalMorosos; i++) {
        morososOrdenados[i].mostrar(morososOrdenados[i].getDni());
    }
}
