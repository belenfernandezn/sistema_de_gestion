#include <cstdio>
#include "Profesor.h"

using namespace std;

Profesor::Profesor() {};

void Profesor::setCodProfesor(int codProfesor) { codProfesor = codProfesor; };

void Profesor::setSueldoProfesor(int sueldoProfesor) { Sueldo = sueldoProfesor; };

int Profesor::codProfesor() { return Cod_Profesor; };

int Profesor::sueldoProfesor() { return Sueldo; };

bool Profesor::leerDeDisco(int pos) {
    bool ok;
    FILE *p;
    p = fopen("Profesores.dat", "rb");
    if (p == nullptr) { return false; }
    fseek(p, sizeof(Profesor)*pos, 0);
    ok = fread(this, sizeof(Profesor), 1, p);
    fclose(p);
    return ok;
}

bool Profesor::grabarEnDisco(Profesor profesor) {
    FILE *p;
    p = fopen("Profesores.dat", "ab");
    if (p == nullptr) { return false; }
    fwrite(&profesor, sizeof(Profesor), 1, p);
    fclose(p);
    return true;
}
