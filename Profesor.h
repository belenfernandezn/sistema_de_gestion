#ifndef PROFESOR_H_INCLUDED
#define PROFESOR_H_INCLUDED
#include "Persona.h"

class Profesor : public Persona {
private:
    int Cod_Profesor;
    int Sueldo;
public:
    Profesor();

    void setCodProfesor(int codProfesor);

    void setSueldoProfesor(int sueldoProfesor);

    int codProfesor();

    int sueldoProfesor();

    bool leerDeDisco(int pos);

    bool grabarEnDisco(Profesor profesor);

    void mostrarProfesor();
};

#endif // PROFESOR_H_INCLUDED
