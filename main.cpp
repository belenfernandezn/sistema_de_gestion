#include <iostream>
#include <cstdlib>

#include "Funciones.h"

using namespace std;

int main() {
    system("cls");
    while(true){
        system("cls");
        int opcion;
        cout << "Gimnasio CHAU CHAU GRASITA" << endl;
        cout << "------------------------------" << endl << endl;
        cout << "1- Menu alumnos" << endl;
        cout << "2- Menu profesores" << endl;
        cout << "3- Turnos" << endl;
        cout << "0- Salir" << endl;
        cout << endl;
        cout << "Seleccione una opcion: ";
        cin >> opcion;

        switch(opcion) {
            case 1:
                menuAlumno();
                break;
            case 2:
                menuProfesor();
                break;
            case 3:
                break;
            case 0:
                return 0;
                break;
            default:
                cout << "La opcion ingresada no es valida";
                system("pause");
                break;
        }
    }
}
