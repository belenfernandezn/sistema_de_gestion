//Librerias
#include <iostream>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include "Fecha.h"

//**CLASS IMPLEMENTATION**//
using namespace std;

//Inicio Fecha
        //Constructor
        Fecha::Fecha(){}
        //Setters
        void Fecha::setDia(int val){ Dia = val; }
        void Fecha::setMes(int val){ Mes = val; }
        void Fecha::setAnio(int val){ Anio = val; }
        //Getters
        int Fecha::getDia(){return Dia;}
        int Fecha::getMes(){return Mes;}
        int Fecha::getAnio(){return Anio;}
        //------------------------------------
        void Fecha::mostrar(){cout << Dia << "/" << Mes << "/" << Anio << endl;}
//Fin Fecha
