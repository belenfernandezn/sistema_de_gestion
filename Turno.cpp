#include <cstdio>
#include "Turno.h"

using namespace std;

Turno::Turno() {};

void Turno::setFecha(Fecha val) { fecha = val; }

void Turno::setHorario(int horario) { horario = horario; };

void Turno::setCod_Profesor(int CodProfesor) { Cod_Profesor = CodProfesor; };

void Turno::setCod_Alumno(int CodAlumno) { Cod_Alumno = CodAlumno; };

Fecha Turno::getFecha() { return fecha; }

int Turno::getHorario() { return horario; };

int Turno::getCodigoProfesor() { return Cod_Profesor; };

int Turno::getCodigoAlumno() { return Cod_Alumno; };

bool Turno::leerDeDisco(int pos) {
    bool ok;
    FILE *p;
    p = fopen("Turnos.dat", "rb");
    if (p == nullptr) { return false; }
    fseek(p, sizeof(Turno)*pos, 0);
    ok = fread(this, sizeof(Turno), 1, p);
    fclose(p);
    return ok;
}

bool Turno::grabarEnDisco(Turno turno) {
    FILE *p;
    p = fopen("Turnos.dat", "ab");
    if (p == nullptr) { return false; }
    fwrite(&turno, sizeof(Turno), 1, p);
    fclose(p);
    return true;
}
