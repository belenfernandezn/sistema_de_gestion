#ifndef NOMINA_H_INCLUDED
#define NOMINA_H_INCLUDED

#include "Fecha.h"

class Nomina {
private:
    Fecha fecha;
    int Cod_Profesor;
    int monto;
public:
    Nomina();

    void setFecha(Fecha);

    void setCodProfesor(int codProfesor);

    void setMonto(int monto);

    Fecha getFecha();

    int getCodigoProfesor();

    int getMonto();

    bool leerDeDisco(int pos);

    bool grabarEnDisco();

    void mostrarNomina();
};

#endif // NOMINA_H_INCLUDED
