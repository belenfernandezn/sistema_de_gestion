#ifndef PERSONA_H_INCLUDED
#define PERSONA_H_INCLUDED

class Persona {
private:
    char nombre[30];
    char apellido[30];
    int dni;
    int telefono;
    bool estado;
public:
    Persona();

    void setNombre(char *nombre);

    void setApellido(char *apellido);

    void setDni(int value);

    void setTelefono(int Telefono);

    void setEstado(bool Estado);

    char *getNombre();

    char *getApellido();

    int getDni();

    int getTelefono();

    bool getEstado();

    void cargar();

    void mostrar();
};

#endif // PERSONA_H_INCLUDED
