//Librerias
#include <iostream>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include "Nomina.h"

//**CLASS IMPLEMENTATION**//
using namespace std;

//Inicio Nomina
        //Constructor
        Nomina::Nomina(){};
        //Setters
        void Nomina::setFecha(Fecha val) { fecha = val; }
        void Nomina::setCodProfesor(int codProfesor){ Cod_Profesor = codProfesor;};
        void Nomina::setMonto(int monto){monto = monto;};
        //Getters
        Fecha Nomina::getFecha() {return fecha; }
        int Nomina::getCodigoProfesor(){return Cod_Profesor;};
        int Nomina::getMonto(){return monto;};
        //------------------------------------
        bool Nomina::leerDeDisco(int pos){
            //Local Declaration
            bool ok;

            FILE *p;
            p = fopen("Nomina.dat", "rb");
            if (p == NULL){
                return false;
            }
            fseek(p, sizeof(Nomina) * pos , 0);
            ok = fread(this, sizeof(Nomina) , 1, p);
            fclose(p);
            return ok;
        }

        bool Nomina::grabarEnDisco(){
            //Local Declaration
            bool ok;

            FILE *p;
            p = fopen("Nomina.dat", "ab");
            if (p == NULL){
                return false;
            }
            ok = fwrite(this, sizeof(Nomina), 1, p);
            fclose(p);
            return ok;
        }
//Fin Nomina
