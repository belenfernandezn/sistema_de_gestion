#include <cstdio>
#include <iostream>
#include "Moroso.h"
#include "Alumno.h"

using namespace std;

Moroso::Moroso() {};

void Moroso::setDni(int codigo) { dni = codigo; };

void Moroso::setDeuda(int cant) { deuda = cant; };

void Moroso::setCantMeses(int cant) { cantMeses = cant; };

int Moroso::getDni() { return dni; };

int Moroso::getDeuda() { return deuda; };

int Moroso::getCantMeses() { return cantMeses; };

void Moroso::mostrar(int morosoDni) {
    Alumno alumno;
    int pos = 0;

    while(alumno.leerDeDisco(pos++)) {
        if(alumno.getDni() == morosoDni) {
            alumno.mostrar();
            cout << deuda << endl;
        }
    }
}

bool Moroso::leerDeDisco(int pos) {
    bool ok;
    FILE *p;
    p = fopen("Morosos.dat", "rb");
    if (p == nullptr) { return false; }
    fseek(p, sizeof(Moroso)*pos, 0);
    ok = fread(this, sizeof(Moroso), 1, p);
    fclose(p);
    return ok;
}

bool Moroso::grabarEnDisco(Moroso moroso) {
    FILE *p;
    p = fopen("Morosos.dat", "ab");
    if (p == nullptr) { return false; }
    fwrite(&moroso, sizeof(Moroso), 1, p);
    fclose(p);
    return true;
}
