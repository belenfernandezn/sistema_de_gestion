#ifndef FECHA_H_INCLUDED
#define FECHA_H_INCLUDED
//**CLASS DEFINITION**//
//**Class Fecha**
class Fecha{
    private:
        int Dia;
        int Mes;
        int Anio;
    public:
        //Constructor
        Fecha();
        //Setters
        void setDia(int val);
        void setMes(int val);
        void setAnio(int val);
        //Getters
        int getDia();
        int getMes();
        int getAnio();
        //Files
        bool LeerDeDisco(int pos);
        bool GrabarEnDisco();
        void mostrar();
};

#endif // FECHA_H_INCLUDED
